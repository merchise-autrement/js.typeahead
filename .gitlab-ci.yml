---
variables:
  PYTHON_VERSION: "3.10"
  PYTHON_IMAGE: "python:$PYTHON_VERSION"
  PIP_NO_CACHE_DIR: "off"
  PIP_DISABLE_PIP_VERSION_CHECK: "on"
  PIP_DEFAULT_TIMEOUT: "100"
  PYTHONDONTWRITEBYTECODE: "1"
  PYTHONUNBUFFERED: "1"
  PYTHONFAULTHANDLER: "1"
  POETRY_VIRTUALENVS_IN_PROJECT: "true"
  POETRY_NO_INTERACTION: "1"

stages:
  - check
  - build
  - test
  - publish

code smells:
  interruptible: true
  image: $PYTHON_IMAGE
  tags:
    - docker
  stage: check
  script:
    - |
      pip install poetry
      poetry install
      poetry run black --check js/
      poetry run isort -c --df js
  cache:
    key: "$CI_PROJECT_ID - $CI_JOB_NAME"
    paths:
      - .venv
  only:
    - main
    - merge_requests

.build: &with-build-dist
  image: $PYTHON_IMAGE
  tags:
    - docker
  stage: build
  script:
    - |
      pip install poetry
      poetry build
  artifacts:
    paths:
      - dist/*.whl
      - dist/*.tar.gz


build wheel and sdist:
  <<: *with-build-dist
  needs: []
  interruptible: true
  only:
    - main
    - merge_requests

build wheel and sdist for releases:
  <<: *with-build-dist
  only:
    - tags

package tests:
  interruptible: true
  image: alpine
  tags:
    - docker
  stage: build
  needs: []
  script:
    - |
      tar -cf tests.tar tests/
  only:
    - main
    - merge_requests
  artifacts:
    paths:
      - tests.tar


test package:
  interruptible: true
  image: python:$PYTHON_VERSION
  tags:
    - docker
  parallel:
    matrix:
      - PYTHON_VERSION: ["3.8", "3.9", "3.10"]
  stage: test
  variables:
    GIT_STRATEGY: "none"
  script:
    - |
      pip install dist/js.typeahead*.whl
      pip install pytest xotl.tools
      tar -xf tests.tar
      pytest tests
  only:
    - main
    - merge_requests
  needs:
    - build wheel and sdist
    - package tests


check signature of tag:
  image: alpine
  tags:
    - docker
  stage: check
  script:
    - apk add gnupg git
    - cat $GNUPG_KEY_FILE | gpg --import
    - git verify-tag $CI_COMMIT_REF_NAME
  only:
    - tags

publish in pypi:
  image: $PYTHON_IMAGE
  tags:
    - docker
  stage: publish
  variables:
    GIT_STRATEGY: "none"
  script:
    - pip install twine
    - twine upload --skip-existing -u "$PYPI_USERNAME" -p "$PYPI_PASSWORD" dist/*
  only:
    - tags
  needs:
    - build wheel and sdist for releases
    - check signature of tag
