import unittest

from xotl.tools.objects import import_object


class TestLibrary(unittest.TestCase):
    def setUp(self):
        self.bloodhound = import_object("js.typeahead:bloodhound_js")
        self.typeahead = import_object("js.typeahead:typeahead_js")
        self.bundle = import_object("js.typeahead:typeahead_bundle_js")

    def test_we_can_need_bloodhound(self):
        self.bloodhound.need()

    def test_we_can_need_typeahead(self):
        self.typeahead.need()

    def test_we_can_need_bundle(self):
        self.bundle.need()
